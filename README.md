# geoip_from_cities

[![npm version](https://badge.fury.io/js/geoip_from_cities.svg)](https://badge.fury.io/js/geoip_from_cities)

Figure out the latittude and longitude of a city in a country.  Doesn't use the error prone google geoip lookup. The google's geo tools have limitation on number 2500 requests per day. 

This bites us periodically, if wish to stay away from the above, then you are at the right spot. 

We wanted an easy way for users to do fuzzy search (simple) by typing the city + country and then visualize them in the globe based on lattitude and longitude.


So this might be opionated..


# How to use this npm 

This npm used ramda.  If you are functional, then we recommend you try it.

## Knowledge of ramda is a plus

[ramda](https://ramdajs.com/docs/)

## fillWithGeoInfo

This function when given inputs  `city`, and `ISO country code` will provide the lattitude and longitude.

Note the ISO country code is 2 characters.

This looks for an exact match. So the recommended way is to do a *locateCity* and then call **fillWithGeoInfo**.

```

const   RioGeo   = require('geoip_from_cities');

const G =  new RioGeo({});

console.log("1. LIVE Delhi, IN");
const t = G.fillWithGeoInfo("Delhi", "IN");
console.log(JSON.stringify(t));

```

## locateCity

This function when given inputs `city (or) few character of a city name`, and `ISO country code` will provide the matches.

Note the ISO country code is 2 characters.

This returns as many results as possible.

```

const   RioGeo   = require('geoip_from_cities');

const G =  new RioGeo({});

console.log("1. LIVE Delhi, IN");
const t = G.locateCity("Delhi", "IN");
console.log(JSON.stringify(t));

```

###  Refer test.js for more test cases. 


This npm uses the cities.json and groups the cites + countries.

The results are cached.

## To get a live result always


```

const   RioGeo   = require('geoip_from_cities');

const G =  new RioGeo({});

console.log("1. LIVE Delhi, IN");
const t = G.locateCity("Delhi", "IN", true);
console.log(JSON.stringify(t));

```

## Development

Edit files in the `src` directory.

Finally the functions exported are in `src/index.js` as declared in `main` of `package.json`. 

The files in `lib/*.js` are transpiled by babel.

```

yarn install

node test.js

node bench.js

```

# Publishing

- To publish newer versions, update the `version` in package.json

- Create an userid in `npmjs.com`.


```

yarn login
###  Type the registered email for npmjs.com

yarn publish 

###  Type the password for npmjs.com


```

# License

MIT
